﻿using Nike.Models.ViewModels;
using Nike.Service.Services;
using System.Web.Mvc;

namespace Nike.Controllers
{
    public class CartSummaryTotalController : Controller
    {
        private CartService cartService;

        public CartSummaryTotalController()
        {
            cartService = new CartService();
        }

        // GET: CartSummaryTotal
        public ActionResult Index()
        {
            return PartialView("_CartSummary", new CartSummaryTotalViewModel(cartService.GetTotalPrice()));
        }
    }
}
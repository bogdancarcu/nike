﻿using Nike.Models.ViewModels;
using Nike.Service.Services;
using System.Web.Mvc;

namespace Nike.Controllers
{
    public class MiniCartController : Controller
    {
        private CartService cartService;
        private GeneralService generalService;

        public MiniCartController()
        {
            generalService = new GeneralService();
            cartService = new CartService();
        }

        public ActionResult Index()
        {
            return PartialView("_MiniCart", new MiniCartViewModel(cartService.GetTotalNumberOfItemsInCart(), cartService.GetTotalPrice(), generalService.GetCurrentUserUsername()));
        }
    }
}
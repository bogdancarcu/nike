﻿using Nike.Models;
using Nike.Models.ViewModels;
using Nike.Service.Services;
using System.Web.Mvc;

namespace Nike.Controllers
{
    public class LayoutController : Controller
    {
        private CartService cartService;
        private GeneralService generalService;

        public LayoutController()
        {
            cartService = new CartService();
            generalService = new GeneralService();
        }

        public ActionResult NavBar()
        {
            return PartialView("_NavBar", new MenuViewModel { VisiblePages = generalService.GetVisiblePages() });
        }

        public JsonResult MiniCart()
        {
            cartService = new CartService();
            return Json(new MiniCartViewModel(cartService.GetTotalNumberOfItemsInCart(), cartService.GetTotalPrice(), generalService.GetCurrentUserUsername()));
        }

        public ActionResult Footer()
        {
            return PartialView("~/Views/FooterBlock/Index.cshtml", generalService.GetPageFooter());
        }

        public ActionResult Header()
        {
            return PartialView("~/Views/HeaderBlock/Index.cshtml", generalService.GetPageHeader());
        }
        
        public JsonResult RedirectToShoppingCartPage()
        {
            return Json(new { url = generalService.GetShoppingCartPage().LinkURL });
        }

        public JsonResult RedirectToLoginPage()
        {
            return Json(new { url = generalService.GetLogInPage().LinkURL });
        }
    }
}
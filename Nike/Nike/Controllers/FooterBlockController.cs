﻿using EPiServer.Web.Mvc;
using Nike.Models.Blocks;
using System.Web.Mvc;

namespace Nike.Controllers
{
    public class FooterBlockController : BlockController<FooterBlock>
    {
        public override ActionResult Index(FooterBlock currentBlock)
        {
            return PartialView(currentBlock);
        }

    }
}

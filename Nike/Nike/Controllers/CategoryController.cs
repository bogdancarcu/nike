﻿using EPiServer.Web.Mvc;
using EPiServer.Web.Routing;
using Nike.Models.Catalog;
using Nike.Models.ViewModels;
using Nike.Service.Services;
using System.Web.Mvc;

namespace Nike.Controllers
{
    public class CategoryController : ContentController<NikeCategory>
    {
        public ActionResult Index()
        {
              var contentLinks = StaticInstanceLocator.ContentRepository.GetDescendents(StaticInstanceLocator.ServiceLocator.GetInstance<IPageRouteHelper>().ContentLink);
              CategoriesViewModel categoriesViewModel = new CategoriesViewModel(contentLinks);
              return View(categoriesViewModel);
        }
    }
}
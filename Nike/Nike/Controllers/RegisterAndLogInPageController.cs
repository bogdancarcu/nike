﻿using EPiServer.Web.Mvc;
using Nike.Helpers;
using Nike.Models.Pages;
using Nike.Models.ViewModels;
using Nike.Service.Services;
using System.Web.Mvc;
using System.Web.Security;

namespace Nike.Controllers
{
    public class RegisterAndLogInPageController : PageController<RegisterAndLogInPage>
    {
        private LoginService loginService;
        private BreadcrumbService breadcrumbService;

        public RegisterAndLogInPageController()
        {
            loginService = new LoginService();
            breadcrumbService = new BreadcrumbService();
        }

        public ActionResult Index(RegisterAndLogInPage currentPage)
        {
            return (loginService.IsAuthenticated()) ? (ActionResult)Redirect(breadcrumbService.GetStartPageUrl()) : View(RegisterLoginViewModelFactory.Create(currentPage));
        }

        public ActionResult _Register(RegisterViewModel registerViewModel)
        {
            return PartialView("~/Views/Shared/_Register.cshtml", registerViewModel);
        }

        public ActionResult _Login(LoginViewModel loginViewModel)
        {
            return PartialView("~/Views/Shared/_Login.cshtml", loginViewModel);
        }

        [HttpPost]
        public ActionResult Register([Bind(Include = "FirstName,LastName,PhoneNumber,Email,Gender,State,Password,ConfirmPassword")] RegisterViewModel registerViewModel)
        {
            if (ModelState.IsValid)
            {
                loginService.RegisterUser(registerViewModel);
                loginService.AuthenticateUser(registerViewModel.Email);
                return Json(new { redirectTo = breadcrumbService.GetStartPageUrl() });
            }
            else
            {
                return PartialView("~/Views/Shared/_Register.cshtml", registerViewModel);
            }
        }

        [HttpPost]
        public ActionResult Login([Bind(Include = "Email,Password")] LoginViewModel loginViewModel)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(loginViewModel.Email, loginViewModel.Password))
                {
                    loginService.AuthenticateUser(loginViewModel.Email);
                    return Json(new { redirectTo = breadcrumbService.GetStartPageUrl() });
                }
                else
                {
                    loginViewModel.IncompatibleFieldsError = "Invalid password for the provided email";
                }
            }
            return PartialView("~/Views/Shared/_Login.cshtml", loginViewModel);
        }

        [HttpPost]
        public void SignOut()
        {
            loginService.LogOutUser(HttpContext.Session);
        }

    }
}
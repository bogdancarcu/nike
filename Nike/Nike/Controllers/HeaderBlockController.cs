﻿using EPiServer.Web.Mvc;
using Nike.Models.Blocks;
using System.Web.Mvc;

namespace Nike.Controllers
{
    public class HeaderBlockController : BlockController<HeaderBlock>
    {
        public override ActionResult Index(HeaderBlock currentBlock)
        {
            return PartialView(currentBlock);
        }
    }
}

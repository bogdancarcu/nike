﻿using EPiServer.Web.Mvc;
using Nike.Models.Blocks;
using System.Web.Mvc;

namespace Nike.Controllers
{
    public class GalleryBlockController : BlockController<GalleryBlock>
    {
        public override ActionResult Index(GalleryBlock currentBlock)
        {
            return PartialView(currentBlock);
        }
    }
}

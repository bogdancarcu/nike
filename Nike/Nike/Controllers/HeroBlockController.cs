﻿using EPiServer.DataAnnotations;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Web.Mvc;
using Nike.Models.Blocks;
using System.Web.Mvc;

namespace Nike.Controllers
{
    [ContentType]
    public class HeroBlockController : BlockController<HeroBlock>
    {
        public override ActionResult Index(HeroBlock currentBlock)
        {
            return PartialView(currentBlock);
        }
    }

    [TemplateDescriptor(Tags = new string[] { "Gallery" })]
    public partial class HeroBlockGalleryController : BlockController<HeroBlock>
    {
        public override ActionResult Index(HeroBlock currentBlock)
        {
            return PartialView(currentBlock);
        }
    }
}

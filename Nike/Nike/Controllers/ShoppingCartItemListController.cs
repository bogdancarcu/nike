﻿using Nike.Helpers;
using Nike.Models.ViewModels.ShoppingCartViewModel;
using Nike.Service.Services;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Nike.Controllers
{
    public class ShoppingCartItemListController : Controller
    {
        private CartService cartService;

        public ShoppingCartItemListController()
        {
            cartService = new CartService();
        }

        public ActionResult ShoppingListView() {
            List<ShoppingCartItemViewModel> shoppingCartItems = cartService.GetAllShoppingCartItems();
            return PartialView("_ShoppingCartItemList", new ShoppingCartItemListViewModel(shoppingCartItems));
        }

        public JsonResult RemoveItem(string skuCode)
        {
            cartService.RemoveFromCart(skuCode);
            return Json(new { Code = skuCode, Price = GetTotalPrice() });
        }


        public JsonResult GetTotalPriceCartSummary()
        {
            return Json(new { TotalPrice = GetTotalPrice() });
        }

        public JsonResult UpdateItemQuantity(string skuCode, string quantity)
        {
            cartService.UpdateQuantity(skuCode, Decimal.Parse(quantity));
            return Json(new { Quantity = Decimal.Parse(quantity), Price = cartService.GetPriceForItem(skuCode), TotalPrice = GetTotalPrice()});
        }

        public decimal GetTotalPrice()
        {
            return cartService.GetTotalPrice();
        }
        

    }
}
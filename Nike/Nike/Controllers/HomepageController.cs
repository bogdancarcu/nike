﻿using EPiServer.Web.Mvc;
using Nike.Models.Pages;
using System.Web.Mvc;
namespace Nike.Controllers
{
    public class HomepageController : PageController<Homepage>
    {
        public ActionResult Index(Homepage currentPage)
        {
            return View(currentPage);
        }
    }
}
﻿using EPiServer.Web.Mvc;
using Nike.Models.Pages;
using System.Web.Mvc;

namespace Nike.Controllers
{
    public class StandardPageController : PageController<StandardPage>
    {
        public ActionResult Index(StandardPage currentPage)
        {
            return View(currentPage);
        }
    }
}
﻿using EPiServer.Web.Mvc;
using Nike.Models.Pages;
using System.Web.Mvc;

namespace Nike.Controllers
{
    public class ShoppingCartPageController : PageController<ShoppingCartPage>
    {
   
        public ActionResult Index(ShoppingCartPage currentPage)
        {
            return View(currentPage);
        }
    }
}
﻿using EPiServer.Web.Mvc;
using Nike.helpers;
using Nike.Models.Catalog;
using Nike.Models.ViewModels.ProductViewModels;
using Nike.Models.ViewModels.VariantViewModels;
using Nike.Service.Services;
using System.Web.Mvc;

namespace Nike.Controllers
{
    public class ShoeProductController : ContentController<ShoeProduct>
    {
        private CartService cartService;
        private ProductService productService;

        public ShoeProductController()
        {
            cartService = new CartService();
            productService = new ProductService();
        }

        public ActionResult Index(ShoeProduct currentContent)
        {
            return View(ShoeProductViewModelFactory.Create(currentContent, cartService.GetVariants(currentContent), productService.GetShoeSkuPrices(cartService.GetVariants(currentContent))));
        }

        [HttpPost]
        public ActionResult DisplayCurrentVariants(ShoeProductViewModel shoeProductViewModel, string color)
        {
            var shoeVariantViewModel = cartService.GetVariantOfColor(shoeProductViewModel.Variants, color);
            return (shoeVariantViewModel != null) ? PartialView("~/Views/ShoeProduct/DisplayCurrentVariants.cshtml", new CurrentVariantViewModel(shoeProductViewModel, shoeVariantViewModel)) : null;
        }

        [HttpPost]
        public ActionResult DisplayCurrentSizeVariant(CurrentVariantViewModel currentViewModel, string size)
        {
            var shoeVariantViewModel = cartService.GetVariantOfSize(currentViewModel.Variants, size);
            return (shoeVariantViewModel != null) ? PartialView("~/Views/ShoeProduct/DisplayCurrentVariants.cshtml", new CurrentVariantViewModel(currentViewModel, shoeVariantViewModel)) : null;
        }

        [HttpPost]
        public ActionResult AddToCart(string skuCode)
        {
            cartService.AddToCart(skuCode);
            return null;
        }

    }

}
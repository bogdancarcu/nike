﻿using EPiServer;
using EPiServer.Security;
using EPiServer.ServiceLocation;
using Mediachase.Commerce.Catalog;
using Mediachase.Commerce.Pricing;

namespace Nike.Service.Services
{
    public class StaticInstanceLocator
    {
        public static IServiceLocator ServiceLocator = EPiServer.ServiceLocation.ServiceLocator.Current;
        public static IContentLoader ContentLoader = ServiceLocator.GetInstance<IContentLoader>();
        public static IContentRepository ContentRepository = ServiceLocator.GetInstance<IContentRepository>();
        public static IPriceService PriceService = ServiceLocator.GetInstance<IPriceService>();
        public static System.Web.Mvc.UrlHelper UrlHelper = ServiceLocator.GetInstance<System.Web.Mvc.UrlHelper>();
        public static ReferenceConverter ReferenceConverter = ServiceLocator.GetInstance<ReferenceConverter>();
        public static IUserImpersonation UserImpersonation = ServiceLocator.GetInstance<IUserImpersonation>();
    }
}
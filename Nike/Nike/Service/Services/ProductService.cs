﻿using EPiServer.Commerce.SpecializedProperties;
using EPiServer.Web.Mvc.Html;
using Mediachase.Commerce;
using Nike.helpers;
using Nike.Models.Catalog;
using Nike.Models.ViewModels.VariantViewModels;
using System.Collections.Generic;
using System.Linq;

namespace Nike.Service.Services
{
    public class ProductService
    {
        private GeneralService generalService;

        public ProductService()
        {
            generalService = new GeneralService();
        }

        public List<ShoeVariantViewModel> GetShoeVariantViewModel(List<ShoeVariantPriceViewModel> shoeSkuPrices)
        {
            List<ShoeVariantViewModel> variants = new List<ShoeVariantViewModel>();
            foreach (ShoeVariantPriceViewModel shoeSkuPrice in shoeSkuPrices)
            {

                ShoeVariantViewModel shoeVariantViewModel = new ShoeVariantViewModel(shoeSkuPrice.ShoeSku, shoeSkuPrice.Price);
                variants.Add(shoeVariantViewModel);
            }
            return variants;
        }

        public List<decimal> ComputePriceRange(List<ShoeVariantPriceViewModel> shoeSkuPrices)
        {
            List<Money> prices = new List<Money>();

            foreach (ShoeVariantPriceViewModel shoeSkuPrice in shoeSkuPrices)
            {
                prices.Add(shoeSkuPrice.Price);
            }
            if (prices.Count >= 2)
                return new List<decimal> { prices.Min().Amount, prices.Max().Amount };
            else
                 if (prices.Count == 1)
                return new List<decimal> { prices[0] };
            else
                return new List<decimal>();
        }

        public List<string> GetProductAssets(ShoeProduct shoeProduct)
        {
            List<string> Assets = new List<string>();
            foreach (CommerceMedia c in shoeProduct.CommerceMediaCollection)
            {
                var friendlyUrl = StaticInstanceLocator.UrlHelper.ContentUrl(c.AssetLink);
                Assets.Add(friendlyUrl);
            }
            return Assets;
        }

        public List<ShoeVariantPriceViewModel> GetShoeSkuPrices(List<ShoeVariant> shoeSkus)
        {
            List<ShoeVariantPriceViewModel> shoeSkuPrices = new List<ShoeVariantPriceViewModel>();
            foreach (ShoeVariant shoeSku in shoeSkus)
            {
                var defaultprice = generalService.GetDefaultPrice(shoeSku.Code);
                if (defaultprice != null)
                {
                    shoeSkuPrices.Add(new ShoeVariantPriceViewModel(shoeSku, defaultprice.UnitPrice));
                } else
                {
                    shoeSkuPrices.Add(new ShoeVariantPriceViewModel(shoeSku, new Money(0, Currency.USD)));
                }
            }
            return shoeSkuPrices;
        }
    }
}
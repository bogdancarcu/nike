﻿using EPiServer.Commerce.SpecializedProperties;
using EPiServer.Core;
using EPiServer.Security;
using EPiServer.Web.Mvc.Html;
using Mediachase.Commerce;
using Mediachase.Commerce.Catalog;
using Mediachase.Commerce.Customers;
using Mediachase.Commerce.Pricing;
using Nike.Models.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;

namespace Nike.Service.Services
{
    public class GeneralService
    {

        public string GetCurrentUserUsername()
        {
            CustomerContext customerContext = new CustomerContext();
            var username = PrincipalInfo.CurrentPrincipal.Identity.Name;
            var user = Membership.GetUser(username);
            if (user != null)
            {
                CustomerContact customerContact = customerContext.GetContactForUser(user);
                return customerContact.FirstName + " " + customerContact.LastName;
            }
            else
            {
                return "GUEST";
            }
        }

        public List<Tuple<string, string>> GetVisiblePages()
        {
            var topLevelPages = StaticInstanceLocator.ContentLoader.GetChildren<PageData>(ContentReference.StartPage);
            var pagesQuery = topLevelPages.Where(pd => pd.VisibleInMenu).Select(pd => new Tuple<string, string>(pd.LinkURL, pd.Name));
            return pagesQuery.ToList();
        }

        public BlockData GetPageFooter()
        {
            var homepage = StaticInstanceLocator.ContentLoader.Get<Homepage>(ContentReference.StartPage);
            return homepage.Footer;
        }

        public BlockData GetPageHeader()
        {
            var homepage = StaticInstanceLocator.ContentLoader.Get<Homepage>(ContentReference.StartPage);
            return homepage.Header;
        }

        public IPriceValue GetDefaultPrice(string code)
        {
            return StaticInstanceLocator.PriceService.GetDefaultPrice(MarketId.Default, DateTime.Now, new CatalogKey(code), Currency.USD);
        }

        public List<string> GetStringUrlsFromMediaCollection(ItemCollection<CommerceMedia> commerceMediaCollection)
        {
            List<string> toReturn = new List<string>();
            foreach (CommerceMedia c in commerceMediaCollection)
            {
                toReturn.Add(StaticInstanceLocator.UrlHelper.ContentUrl(c.AssetLink));
            }
            return toReturn;
        }

        internal ShoppingCartPage GetShoppingCartPage()
        {
            var allPages = StaticInstanceLocator.ContentLoader.GetChildren<PageData>(ContentReference.StartPage);
            ShoppingCartPage shoppingCartPage = null;
            foreach (PageData pageData in allPages)
            {
                if (pageData is ShoppingCartPage)
                {
                    shoppingCartPage = (ShoppingCartPage)pageData;
                }
            }
            return shoppingCartPage;
        }

        internal RegisterAndLogInPage GetLogInPage()
        {
            var allPages = StaticInstanceLocator.ContentLoader.GetChildren<PageData>(ContentReference.StartPage);
            RegisterAndLogInPage shoppingCartPage = null;
            foreach (PageData pageData in allPages)
            {
                if (pageData is RegisterAndLogInPage)
                {
                    shoppingCartPage = (RegisterAndLogInPage)pageData;
                }
            }
            return shoppingCartPage;
        }
    }
}
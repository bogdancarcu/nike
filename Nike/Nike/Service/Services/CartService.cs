﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Commerce.Order;
using EPiServer.Core;
using EPiServer.Security;
using EPiServer.ServiceLocation;
using Mediachase.Commerce.Catalog;
using Mediachase.Commerce.Security;
using Nike.Helpers;
using Nike.Models.Catalog;
using Nike.Models.ViewModels.VariantViewModels;
using System.Collections.Generic;
using System.Linq;

namespace Nike.Service.Services
{
    public class CartService
    {
        private IOrderRepository orderRepository;
        private ICart cart;
        private GeneralService generalService;

        public CartService()
        {
            var currentUserId = PrincipalInfo.CurrentPrincipal.GetContactId();
            orderRepository = ServiceLocator.Current.GetInstance<IOrderRepository>();
            cart = orderRepository.LoadOrCreateCart<ICart>(currentUserId, "Default");
            generalService = new GeneralService();
            RefreshCart(); 
        }
       
        public void AddToCart(string skuCode)
        {
            var lineItem = cart.GetAllLineItems().FirstOrDefault(x => x.Code == skuCode);

            if (lineItem == null)
            {
                lineItem = cart.CreateLineItem(skuCode);
                lineItem.Quantity = 1;
                cart.AddLineItem(lineItem);
            }
            else
            {
                var shipment = cart.GetFirstShipment();
                cart.UpdateLineItemQuantity(shipment, lineItem, lineItem.Quantity + 1);
            }

            orderRepository.Save(cart);
        }

        public void RemoveFromCart(string skuCode)
        {
            var lineItem = cart.GetAllLineItems().FirstOrDefault(x => x.Code == skuCode);

            if (lineItem != null)
            {
                var shipment = cart.GetFirstShipment();
                shipment.LineItems.Remove(lineItem);
            }

            orderRepository.Save(cart);
        }

 
        public List<ILineItem> GetAllCartItems()
        {
            return cart.GetAllLineItems().ToList();
        }

        public void UpdateQuantity(string skuCode, decimal newQuantity)
        {
            var lineItem = cart.GetAllLineItems().FirstOrDefault(x => x.Code == skuCode);

            if (lineItem != null)
            {
                var shipment = cart.GetFirstShipment();
                cart.UpdateLineItemQuantity(shipment, lineItem, newQuantity);
            }
    
            orderRepository.Save(cart);
        }

        public decimal GetTotalNumberOfItemsInCart()
        {
            decimal total = 0;
            foreach (var lineItem in GetAllCartItems())
            {
                total += lineItem.Quantity;
            }
            return total;
        }

        public decimal GetTotalPrice()
        {
            decimal total = 0;
            foreach (var lineItem in GetAllCartItems())
            {
               total += lineItem.Quantity * generalService.GetDefaultPrice(lineItem.Code).UnitPrice.Amount;
            }
            return total;
        }

        public decimal GetPriceForItem(string code)
        {
            return generalService.GetDefaultPrice(code).UnitPrice.Amount;
        }

        public List<ShoeVariant> GetVariants(ShoeProduct currentContent)
        {
            List<ShoeVariant> shoeSkus = new List<ShoeVariant>();
            foreach (ContentReference contentReference in currentContent.GetVariants())
            {
                shoeSkus.Add(StaticInstanceLocator.ContentRepository.Get<ShoeVariant>(contentReference));
            }
            return shoeSkus;
        }

        public ShoeVariantViewModel GetVariantOfColor(List<ShoeVariantViewModel> variants, string color)
        {
            foreach (ShoeVariantViewModel shoeVariantViewModel in variants)
            {
                if (shoeVariantViewModel.Color1.Equals(color))
                {
                    return shoeVariantViewModel;
                }
            }
            return null;
        }

        public ShoeVariantViewModel GetVariantOfSize(List<ShoeVariantViewModel> variants, string size)
        {
            foreach (ShoeVariantViewModel shoeVariantViewModel in variants)
            {
                if (shoeVariantViewModel.Size.Equals(size))
                {
                    return shoeVariantViewModel;
                }
            }
            return null;
        }


        private void RefreshCart()
        {
            var referenceConverter = ServiceLocator.Current.GetInstance<ReferenceConverter>();
            var repo = StaticInstanceLocator.ContentRepository;
            var cartLineItems = cart.GetAllLineItems();
            IList<ILineItem> threats = new List<ILineItem>();

            foreach (var item in cartLineItems)
            {
                var variantLink = referenceConverter.GetContentLink(item.Code);

                try
                {
                    var variant = repo.Get<VariationContent>(variantLink);
                    var parentReference = variant.GetParentProducts().FirstOrDefault();
                    var parent = repo.Get<ProductContent>(parentReference);
                }
     
                catch(System.ArgumentNullException)
                {
                    threats.Add(item);
                }
            }

            var shipment = cart.GetFirstShipment();
            foreach (var threat in threats)
            {   
                shipment.LineItems.Remove(threat);
            }

            orderRepository.Save(cart);
        }

        public List<ShoppingCartItemViewModel> GetAllShoppingCartItems()
        {
            List<ShoppingCartItemViewModel> list = new List<ShoppingCartItemViewModel>();
            var cartLineItems = GetAllCartItems();

            foreach (var item in cartLineItems)
            {
                var variant = StaticInstanceLocator.ContentRepository.Get<VariationContent>(StaticInstanceLocator.ReferenceConverter.GetContentLink(item.Code));
                var parent = StaticInstanceLocator.ContentRepository.Get<ProductContent>(variant.GetParentProducts().FirstOrDefault());
                var shoeSku = (ShoeVariant)variant;
                var shoeProduct = (ShoeProduct)parent;
                var priceOneItem = generalService.GetDefaultPrice(shoeSku.Code).UnitPrice.Amount;

                ShoppingCartItemViewModel newItem = new ShoppingCartItemViewModel
                {
                    ProductName = shoeProduct.ProductName,
                    SelectedColorName = shoeSku.Color1,
                    SelectedColorCode = shoeSku.ColorCode,
                    SelectedSize = shoeSku.Size,
                    PriceOneItem = priceOneItem,
                    Quantity = item.Quantity.ToString(),
                    ImageUrl = generalService.GetStringUrlsFromMediaCollection(shoeSku.CommerceMediaCollection).FirstOrDefault(),
                    SkuCode = item.Code,
                    TotalPrice = item.Quantity * priceOneItem
                };

                list.Add(newItem);
            }
            return list;
        }

    }
}
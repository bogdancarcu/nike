﻿using EPiServer.Core;
using EPiServer.Web.Routing;
using Nike.Models.Pages;
using System.Collections.Generic;
using System.Linq;

namespace Nike.Service.Services
{
    public class BreadcrumbService
    {
        public IEnumerable<IContent> GetAncestors(ContentReference contentReference)
        {
            var ancestors = StaticInstanceLocator.ContentRepository.GetAncestors(contentReference);
            var categories = ancestors.Take(ancestors.Count() - 1);
            return categories;
        }

        public List<string> GetAncestorLinks(IEnumerable<IContent> ancestors)
        {
            var toReturn = new List<string>();

            foreach (IContent content in ancestors)
            {
                var url = UrlResolver.Current.GetUrl(content.ContentLink);
                toReturn.Insert(0, url);
            }
            return toReturn;
        }

        public List<string> GetAncestorNames(IEnumerable<IContent> ancestors)
        {
            var toReturn = new List<string>();
            foreach (IContent content in ancestors)
            {
                toReturn.Insert(0, content.Name);
            }
            return toReturn;
        }

        public string GetStartPageUrl()
        {
            var page = StaticInstanceLocator.ContentRepository.Get<Homepage>(ContentReference.StartPage);
            var homeUrl = UrlResolver.Current.GetUrl(page.ContentLink);
            return homeUrl;
        }
    }
}
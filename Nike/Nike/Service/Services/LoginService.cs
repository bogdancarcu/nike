﻿using Mediachase.Commerce.Customers;
using Nike.Models.ViewModels;
using System.Web;
using System.Web.Security;

namespace Nike.Service.Services
{
    public class LoginService
    {
        public bool IsAuthenticated()
        {
            return (HttpContext.Current.User.Identity.IsAuthenticated);
        }

        public void RegisterUser(RegisterViewModel registerViewModel)
        {
            MembershipUser user;

            user = Membership.CreateUser(registerViewModel.Email, registerViewModel.Password, registerViewModel.Email);

            CustomerContact customerContact = CustomerContact.CreateInstance(user);
            customerContact.FirstName = registerViewModel.FirstName;
            customerContact.LastName = registerViewModel.LastName;
            customerContact["PhoneNumber"] = registerViewModel.PhoneNumber;
            customerContact["State"] = registerViewModel.SelectedState;
            customerContact["Gender"] = registerViewModel.Gender;
            customerContact.SaveChanges();
        }

        public void AuthenticateUser(string email)
        {
            FormsAuthentication.SetAuthCookie(email, true);
        }

        internal void LogOutUser(HttpSessionStateBase session)
        {
            session.Clear();
            session.Abandon();
            session.RemoveAll();
            HttpContext.Current.User = null;
            FormsAuthentication.SignOut();
        }
    }
}
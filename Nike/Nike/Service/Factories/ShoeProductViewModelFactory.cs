﻿using Nike.Models.Catalog;
using Nike.Models.ViewModels.ProductViewModels;
using System.Collections.Generic;

namespace Nike.helpers
{
    public class ShoeProductViewModelFactory
    {
        public static ShoeProductViewModel Create(ShoeProduct shoeProduct, List<ShoeVariant> shoeSkus, List<ShoeVariantPriceViewModel> shoeskuPrices)
        {
            var resultModel = new ShoeProductViewModel(shoeProduct, shoeskuPrices);

            foreach (ShoeVariant shoeSku in shoeSkus)
            {
                if(shoeSku.Color1 != null && shoeSku.ColorCode != null && shoeSku.Size != null){
                    resultModel.Colors.Add(shoeSku.Color1);
                    resultModel.ColorCodes.Add(shoeSku.ColorCode);
                    resultModel.Sizes.Add(shoeSku.Size);
                }
            }
            return resultModel;
        }
    }
}
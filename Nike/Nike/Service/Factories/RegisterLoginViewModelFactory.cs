﻿using Nike.Models.Pages;
using Nike.Models.ViewModels;

namespace Nike.Helpers
{
    public class RegisterLoginViewModelFactory
    {
        public static RegisterLoginViewModel Create(RegisterAndLogInPage registerAndLogInPage)
        {
            RegisterViewModel registerViewModel = new RegisterViewModel();
            LoginViewModel loginViewModel = new LoginViewModel();

            var resultModel = new RegisterLoginViewModel
            {
                Title = registerAndLogInPage.Title,
                LogInSubtitle = registerAndLogInPage.LogInSubtitle,
                RegisterSubtitle = registerAndLogInPage.RegisterSubtitle,
                Gallery = registerAndLogInPage.Gallery,
                RegisterViewModel = registerViewModel,
                LoginViewModel = loginViewModel
            };
            return resultModel;
        }
    }
}
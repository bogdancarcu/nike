﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace Nike.Models.Pages
{
    [ContentType(DisplayName = "Standard Page", GUID = "8faa75ac-4a8b-418e-a09b-bde927f93750", Description = "")]
    public class StandardPage : PageData
    {

        [CultureSpecific]
        [Display(
            Name = "Custom HTML Area",
            Description = "User can input HTML code here.",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        [PropertySettings(typeof(XhtmlString))]
        public virtual XhtmlString CustomHTMLArea { get; set; }

        [CultureSpecific]
        [Display(
           Name = "Content Area for blocks",
           Description = "More blocks and other media can be added here",
           GroupName = SystemTabNames.Content,
           Order = 2)]
        public virtual ContentArea Content { get; set; }

    }
}
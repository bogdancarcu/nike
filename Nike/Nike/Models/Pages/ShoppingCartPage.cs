﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace Nike.Models.Pages
{
    [ContentType(DisplayName = "ShoppingCartPage", GUID = "2f4b93ab-9859-4be0-88ca-a1396f2ea14b", Description = "")]
    public class ShoppingCartPage : PageData
    {

        [CultureSpecific]
        [Display(
            Name = "Shopping Cart Title",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual XhtmlString ShoppingCartTitle { get; set; }

        [CultureSpecific]
        [Display(
              Name = "Cart Summary Title",
              Description = "",
              GroupName = SystemTabNames.Content,
              Order = 2)]
        public virtual XhtmlString CartSummaryTitle { get; set; }

        [CultureSpecific]
        [Display(
          Name = "Shopping Page Empty Text",
          Description = "",
          GroupName = SystemTabNames.Content,
          Order = 3)]
        public virtual XhtmlString TextWhenEmpty { get; set; }

    }
}
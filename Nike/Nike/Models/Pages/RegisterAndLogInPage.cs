﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Nike.Models.Blocks;
using System;
using System.ComponentModel.DataAnnotations;

namespace Nike.Models.Pages
{
    [ContentType(DisplayName = "RegisterAndLogInPage", GUID = "309f2625-f7bd-41a1-bd7a-0a208454a101", Description = "")]
    public class RegisterAndLogInPage : PageData
    {
        
        [CultureSpecific]
        [Display(
            Name = "Title",
            Description = "The title will be shown in the upper part of the Register/Sign in page.",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual String Title { get; set; }

        [CultureSpecific]
        [Display(
        Name = "Register Subtitle",
        Description = "The Register subtitle will be shown in the upper part of the Register area.",
        GroupName = SystemTabNames.Content,
        Order = 2)]
        public virtual String RegisterSubtitle { get; set; }

        [CultureSpecific]
        [Display(
        Name = "Log In Subtitle",
        Description = "The Log In subtitle will be shown in the upper part of the Log In area.",
        GroupName = SystemTabNames.Content,
        Order = 3)]
        public virtual String LogInSubtitle { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Gallery",
            Description = "The Gallery area can hold Gallery Blocks",
            GroupName = SystemTabNames.Content,
            Order = 4)]
        [AllowedTypes(new[] { typeof(GalleryBlock) })]
        public virtual ContentArea Gallery { get; set; }

    }
}
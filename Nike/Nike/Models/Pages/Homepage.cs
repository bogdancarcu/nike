﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using Nike.Models.Blocks;
using System.ComponentModel.DataAnnotations;

namespace Nike.Models.Pages
{
    [ContentType(DisplayName = "Homepage", GUID = "dff420e8-764b-4247-b7da-00e708562f49", Description = "")]
    public class Homepage : PageData
    {
        [CultureSpecific]
        [Display(
            Name = "Content",
            Description = "The main body will be shown in the main content area of" +
            " the page, using the XHTML-editor you can insert for example text, images and tables.",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual ContentArea Content { get; set; }

        [Display(
           Name = "Footer",
           Description = "Footer",
           GroupName = "Footer",
           Order = 2)]
        public virtual FooterBlock Footer { get; set; }

        [Display(
            Name = "Header",
            Description = "Header Area",
            GroupName = "Header",
            Order = 3)]
        public virtual HeaderBlock Header { get; set; }

    }
}
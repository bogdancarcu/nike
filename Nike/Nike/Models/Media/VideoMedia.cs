﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Web;

namespace Nike.Models.Media
{
    [ContentType(DisplayName = "VideoMedia", GUID = "c3a3a927-1b2f-4758-96e4-faac181ce2f7", Description = "")]
    [MediaDescriptor(ExtensionString = "mp4")]
    public class VideoMedia : VideoData
    {

        public virtual string Copyright { get; set; }

        [UIHint(UIHint.Image)]
        public virtual ContentReference PreviewImage { get; set; }

    }
}
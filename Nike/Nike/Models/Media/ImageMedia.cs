﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Framework.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace Nike.Models.Media
{
    [ContentType(DisplayName = "ImageMedia", GUID = "8226e6c4-2a33-4a34-b843-13f29793f0b9", Description = "")]
    [MediaDescriptor(ExtensionString = "jpg, jpeg, png")]
    public class ImageMedia : ImageData
    {
        [CultureSpecific]
        [Editable(true)]
        [Display(
            Name = "Description",
            Description = "Description field's description",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual string Description { get; set; }

    }
}
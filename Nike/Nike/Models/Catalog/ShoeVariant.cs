﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Commerce.Catalog.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace Nike.Models.Catalog
{
    [CatalogContentType(DisplayName = "Shoe Sku",
    GUID = "2959CE29-39BE-4F90-B266-5A04E315C1F8",
    MetaClassName = "ShoeSku")]
    public class ShoeVariant : VariationContent
    {
        [Display(Name = "Shoe Sku Size", Order = 1)]
        public virtual string Size { get; set; }

        [Display(Name = "Shoe Sku Color Name", Order = 2)]
        public virtual string Color1 { get; set; }

        [Display(Name = "Shoe Sku Color Code", Order = 3)]
        public virtual string ColorCode { get; set; }

    }
}
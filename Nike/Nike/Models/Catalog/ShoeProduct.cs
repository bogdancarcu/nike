﻿using EPiServer.Commerce.Catalog.ContentTypes;
using EPiServer.Commerce.Catalog.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace Nike.Models.Catalog
{
    [CatalogContentType(DisplayName = "Shoe",
        GUID = "51DFDE19-DF37-4A26-8BD4-DAC8B5BD6E81",
        MetaClassName = "ShoeProduct")]
    public class ShoeProduct : ProductContent
    {
        [Display(Name = "Product Name", Order = 1)]
        public virtual string ProductName { get; set; }

        [PropertySettings(typeof(XhtmlString))]
        [Display(Name = "Product Description", Order = 2)]
        public virtual XhtmlString Description { get; set; }

    }
}
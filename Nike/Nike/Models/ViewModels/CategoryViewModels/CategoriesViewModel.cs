﻿using EPiServer.Core;
using EPiServer.Web.Mvc.Html;
using EPiServer.Web.Routing;
using Nike.helpers;
using Nike.Models.Catalog;
using Nike.Service.Services;
using System.Collections.Generic;
using System.Linq;

namespace Nike.Models.ViewModels
{
    public class CategoriesViewModel
    {
        public virtual ProductDisplayViewModel CurrentProductDisplay { get; set; }

        public virtual List<ProductDisplayViewModel> ProductDisplayList { get; set; }

        public bool IsFirstVariant { get; set; }


        public CategoriesViewModel(IEnumerable<ContentReference> productCollection)
        {

            ProductDisplayList = new List<ProductDisplayViewModel>();
            foreach (ContentReference contentReference in productCollection)
            {
                if (CheckDataType("Castle.Proxies.ShoeProductProxy", contentReference))
                {
                    ProcessShoeProductData(contentReference);
                }
            }
        }

        public bool CheckDataType(string dataType, ContentReference contentReference)
        {
            return StaticInstanceLocator.ContentRepository.Get<IContentData>(contentReference).GetType().ToString().Equals(dataType);
        }

        public void ProcessShoeProductData(ContentReference contentReference)
        {
            CurrentProductDisplay = new ProductDisplayViewModel();
            ShoeProduct shoeProduct = StaticInstanceLocator.ContentRepository.Get<ShoeProduct>(contentReference);
            CurrentProductDisplay.Href = UrlResolver.Current.GetUrl(contentReference);
            CurrentProductDisplay.ProductName = shoeProduct.ProductName;
            CurrentProductDisplay.AssetUrlLink = GetFirstAssetUrlLink(shoeProduct) == "" ? "../../../images/No_Image_Available.jpg" : GetFirstAssetUrlLink(shoeProduct);
            ProductDisplayList.Add(CurrentProductDisplay);
            IsFirstVariant = true;
        }
 
        public string GetFirstAssetUrlLink(ShoeProduct shoeProduct)
        {
            if (shoeProduct.CommerceMediaCollection.Count == 0)
                return "";
            else
            return StaticInstanceLocator.UrlHelper.ContentUrl(shoeProduct.CommerceMediaCollection.First().AssetLink);
        }
    }
}
﻿using Nike.helpers;
using Nike.Models.Catalog;
using Nike.Models.ViewModels.VariantViewModels;
using Nike.Service.Services;
using System.Collections.Generic;

namespace Nike.Models.ViewModels.ProductViewModels
{
    public class ShoeProductViewModel
    {
        private BreadcrumbService breadcrumbService;
        private ProductService productService;

        public string ShoeProductDescription { get; set; }

        public string ShoeProductName { get; set; }

        public List<ShoeVariantViewModel> Variants { get; set; }

        public decimal MinPrice { get; set; }

        public decimal MaxPrice { get; set; }

        public List<string> Categories { get; set; }

        public List<string> AncestorNames { get; set; }

        public List<string> AncestorLinks { get; set; }

        public string HomeUrl { get; set; }

        public HashSet<string> Colors { get; set; }

        public HashSet<string> ColorCodes { get; set; }

        public SortedSet<string> Sizes { get; set; }

        public List<string> AssetLinks { get; set; }

        public ShoeProductViewModel() { }

        public ShoeProductViewModel(ShoeProduct shoeProduct, List<ShoeVariantPriceViewModel> shoeSkuPrices)
        {
            breadcrumbService = new BreadcrumbService();
            productService = new ProductService();

            AncestorNames = new List<string>();
            AncestorLinks = new List<string>();
            Colors = new HashSet<string>();
            ColorCodes = new HashSet<string>();
            Sizes = new SortedSet<string>();

            ShoeProductDescription = shoeProduct.Description == null ? "" : shoeProduct.Description.ToString() ;
            ShoeProductName = shoeProduct.ProductName;
            BreadcrumbServiceCall(shoeProduct);
            ProductServiceCall(shoeProduct, shoeSkuPrices);
        }

        public void BreadcrumbServiceCall(ShoeProduct shoeProduct)
        {
            var ancestorsContent = breadcrumbService.GetAncestors(shoeProduct.ContentLink);
            AncestorLinks = breadcrumbService.GetAncestorLinks(ancestorsContent);
            AncestorNames = breadcrumbService.GetAncestorNames(ancestorsContent);
            HomeUrl = breadcrumbService.GetStartPageUrl();
        }

        public void ProductServiceCall(ShoeProduct shoeProduct, List<ShoeVariantPriceViewModel> shoeSkuPrices)
        {
            Variants = productService.GetShoeVariantViewModel(shoeSkuPrices);
            List<decimal> priceRange = productService.ComputePriceRange(shoeSkuPrices);
            if(priceRange.Count == 2)
            {
                MinPrice = priceRange[0];
                MaxPrice = priceRange[1];
            }
            else if (priceRange.Count == 1)
            {
                MinPrice = priceRange[0];
                MaxPrice = priceRange[0];
            }

                AssetLinks = productService.GetProductAssets(shoeProduct);
        }
    }
}
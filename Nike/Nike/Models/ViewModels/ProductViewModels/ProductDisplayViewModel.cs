﻿namespace Nike.helpers
{
    public class ProductDisplayViewModel
    {
        public string ProductName { get; set; }

        public string AssetUrlLink { get; set; }

        public string Href { get; set; }
    }
}
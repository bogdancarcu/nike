﻿using Nike.Helpers;
using System.Collections.Generic;

namespace Nike.Models.ViewModels.ShoppingCartViewModel
{
    public class ShoppingCartItemListViewModel
    {
        public List<ShoppingCartItemViewModel> ShoppingCartItemList { get; set; }

        public ShoppingCartItemListViewModel() { }

        public ShoppingCartItemListViewModel(List<ShoppingCartItemViewModel> shoppingCartItemList)
        {
            ShoppingCartItemList = shoppingCartItemList;
        }
    }
}
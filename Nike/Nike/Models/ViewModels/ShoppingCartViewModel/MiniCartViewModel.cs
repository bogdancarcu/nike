﻿namespace Nike.Models.ViewModels
{
    public class MiniCartViewModel
    {
        public decimal NumberOfItems { get; set; }
        public decimal TotalPrice { get; set; }
        public string Username { get; set; }


        public MiniCartViewModel(decimal NumberOfItems, decimal TotalPrice, string Username)
        {
            this.NumberOfItems = NumberOfItems;
            this.TotalPrice = TotalPrice;
            this.Username = Username;
        }
    }
}
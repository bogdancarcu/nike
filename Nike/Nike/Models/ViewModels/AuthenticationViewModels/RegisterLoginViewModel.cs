﻿using EPiServer.Core;
using System;

namespace Nike.Models.ViewModels
{
    public class RegisterLoginViewModel
    {
        public virtual String Title { get; set; }

        public virtual String RegisterSubtitle { get; set; }

        public virtual String LogInSubtitle { get; set; }

        public virtual ContentArea Gallery { get; set; }

        public RegisterViewModel RegisterViewModel { get; set; }

        public LoginViewModel LoginViewModel { get; set; }

    }
}
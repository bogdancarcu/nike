﻿using Nike.Validators;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Nike.Models.ViewModels
{
    public class RegisterViewModel
    {
        [MaxLength(25)]
        [Required(ErrorMessage = "Please input the First Name")]
        public string FirstName { get; set; }

        [MaxLength(25)]
        [Required(ErrorMessage = "Please input the Last Name")]
        public string LastName { get; set; }
        
        [CustomEmailValidator]
        [Required(ErrorMessage = "Please input the Email Address")]
        public string Email { get; set; }

        [CustomPhoneNumberValidator]
        [Required(ErrorMessage = "Please input the Phone Number")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Please select a gender")]
        public string Gender { get; set; }

       // [Required(ErrorMessage = "Please select a state")]
        public string SelectedState { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please input the Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please confirm your Password")]
        [System.ComponentModel.DataAnnotations.Compare("Password")]
        public string ConfirmPassword { get; set; }

        public IEnumerable<string> StateList = new List<string>()
        {
            "Romania",
            "Netherlands",
            "France"
        };
    }
} 
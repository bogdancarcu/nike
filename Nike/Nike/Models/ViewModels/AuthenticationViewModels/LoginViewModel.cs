﻿using Nike.Validators;
using System.ComponentModel.DataAnnotations;

namespace Nike.Models.ViewModels
{
    public class LoginViewModel
    {
        [CustomLoginEmailValidator]
        [Required(ErrorMessage = "Please input the Email Address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please input the Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public string IncompatibleFieldsError { get; set; }

    }
}
﻿namespace Nike.Models.ViewModels
{
    public class CartSummaryTotalViewModel
    {
        public CartSummaryTotalViewModel(decimal totalPrice)
        {
            TotalPrice = totalPrice;
        }
        public decimal TotalPrice { get; set; }
    }
}
﻿namespace Nike.Helpers
{
    public class ShoppingCartItemViewModel
    {
        public string ProductName { get; set; }
        public string SelectedColorName { get; set; }
        public string SelectedColorCode { get; set; }
        public string SelectedSize { get; set; }
        public decimal PriceOneItem { get; set; }
        public decimal TotalPrice { get; set; }
        public string Quantity { get; set; }
        public string ImageUrl { get; set; }

        public string SkuCode { get; set; }

        public ShoppingCartItemViewModel() { }


        public ShoppingCartItemViewModel(string productName, string selectedColorName, string selectedColorCode, string selectedSize, decimal priceOneItem, string quantity, string imageUrl, string skuCode, decimal totalPrice)
        {
            ProductName = productName;
            SelectedColorName = selectedColorName;
            SelectedColorCode = selectedColorCode;
            SelectedSize = selectedSize;
            PriceOneItem = priceOneItem;
            Quantity = quantity;
            TotalPrice = totalPrice;
            ImageUrl = imageUrl;
            SkuCode = skuCode;

        }

    }
}
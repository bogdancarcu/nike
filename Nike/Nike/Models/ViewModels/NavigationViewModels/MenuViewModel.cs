﻿using System;
using System.Collections.Generic;

namespace Nike.Models
{
    public class MenuViewModel
    {
        public virtual List<Tuple<String, String>> VisiblePages { get; set; }
    }
}
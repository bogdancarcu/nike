﻿using Mediachase.Commerce;
using Nike.Models.Catalog;

namespace Nike.helpers
{
    public class ShoeVariantPriceViewModel
    {
        public ShoeVariant ShoeSku { get; set; }
        public Money Price { get; set; }
        public ShoeVariantPriceViewModel(ShoeVariant shoeSku, Money price)
        {
            this.ShoeSku = shoeSku;
            Price = price;
        }
    }
}
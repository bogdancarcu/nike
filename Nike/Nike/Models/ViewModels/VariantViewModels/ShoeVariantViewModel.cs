﻿using Mediachase.Commerce;
using Nike.Models.Catalog;
using Nike.Service.Services;
using System.Collections.Generic;

namespace Nike.Models.ViewModels.VariantViewModels
{
    public class ShoeVariantViewModel
    {
        private GeneralService generalService;

        public ShoeVariantViewModel(ShoeVariant shoeSku, Money price)
        {
            generalService = new GeneralService();
            Size = shoeSku.Size;
            Color1 = shoeSku.Color1;
            ColorCode = shoeSku.ColorCode;
            Price = price.Amount;
            CurrencyCode = price.Currency.CurrencyCode;
            AssetLinks = generalService.GetStringUrlsFromMediaCollection(shoeSku.CommerceMediaCollection);
            Code = shoeSku.Code;
        }

        public ShoeVariantViewModel()
        {
            generalService = new GeneralService();
        }

        public string Code { get; set; }

        public string Color1 { get; set; }

        public string ColorCode { get; set; }

        public string Size { get; set; }

        public decimal Price { get; set; }

        public string CurrencyCode { get; set; }

        public List<string> AssetLinks { get; set; }

    }
}
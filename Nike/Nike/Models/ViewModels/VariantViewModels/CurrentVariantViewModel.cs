﻿using Nike.Models.ViewModels.ProductViewModels;
using System.Collections.Generic;

namespace Nike.Models.ViewModels.VariantViewModels
{
    public class CurrentVariantViewModel
    {
        public string ShoeProductName { get; set; }

        public string ShoeProductDescription { get; set; }

        public List<ShoeVariantViewModel> Variants { get; set; }

        public ShoeVariantViewModel CurrentVariant { get; set; }

        public CurrentVariantViewModel(ShoeProductViewModel shoeProductViewModel, ShoeVariantViewModel shoeVariantViewModel)
        {
            ShoeProductName = shoeProductViewModel.ShoeProductName;
            ShoeProductDescription = shoeProductViewModel.ShoeProductDescription;
            Variants = shoeProductViewModel.Variants;
            CurrentVariant = shoeVariantViewModel;
        }

        //maybe
        public CurrentVariantViewModel(CurrentVariantViewModel currentViewModel, ShoeVariantViewModel shoeVariantViewModel)
        {
            ShoeProductName = currentViewModel.ShoeProductName;
            ShoeProductDescription = currentViewModel.ShoeProductDescription;
            Variants = currentViewModel.Variants;
            CurrentVariant = shoeVariantViewModel;
        }

        public CurrentVariantViewModel()
        {

        }

    }
}
﻿using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using EPiServer.Web;

namespace Nike.Models.Blocks
{
    [ContentType(DisplayName = "Hero Block", GUID = "3f3f8f8f-4fbb-4d92-b588-f7f0bf79290c", Description = "")]
    public class HeroBlock : BlockData
    {

        [Display(
                   Name = "Eyebrow Link",
                   Description = "Eyebrow before title",
                   GroupName = SystemTabNames.Content,
                   Order = 1)]
        [ListItems(1)]
        public virtual LinkItemCollection EyebrowLink { get; set; }

        [Display(
                   Name = "Title",
                   Description = "Title of hero block",
                   GroupName = SystemTabNames.Content,
                   Order = 2)]
        public virtual XhtmlString Title { get; set; }

        [Display(
                   Name = "MediaFile",
                   Description = "MediaFile",
                   GroupName = SystemTabNames.Content,
                   Order = 3)]
        [UIHint(UIHint.MediaFile)]
        public virtual ContentReference Image { get; set; }

        [Display(
                   Name = "Preview Text",
                   Description = "Text preview of hero block",
                   GroupName = SystemTabNames.Content,
                   Order = 4)]
        public virtual XhtmlString PreviewText { get; set; }

        [Display(
                   Name = "Read More",
                   Description = "Used for simple 'Read More' Redirection",
                   GroupName = SystemTabNames.Content,
                   Order = 5)]
        public virtual ContentReference LinkText { get; set; }

    }
}
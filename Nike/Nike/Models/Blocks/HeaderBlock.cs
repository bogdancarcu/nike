﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using System.ComponentModel.DataAnnotations;

namespace Nike.Models.Blocks
{
    [ContentType(DisplayName = "HeaderBlock", GUID = "9c7f3fa9-950a-4ad4-8501-fd340ade4b85", Description = "")]
    public class HeaderBlock : BlockData
    {
        [CultureSpecific]
        [Display(
           Name = "Categories",
           Description = "Add links to category pages, these would be displayed on the navbar within header",
           GroupName = SystemTabNames.Content,
           Order = 1)]
        public virtual LinkItemCollection Categories { get; set; }
    }
}
﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using System.ComponentModel.DataAnnotations;

namespace Nike.Models.Blocks
{
    [ContentType(DisplayName = "Gallery Block", GUID = "6f9abd0c-768f-4717-9a90-a77e646f2fb3", Description = "")]
    public class GalleryBlock : BlockData
    {

        [CultureSpecific]
        [Display(
            Name = "Container",
            Description = "Can add/delete/reorder hero blocks",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        [AllowedTypes(typeof(HeroBlock))]
        public virtual ContentArea Container { get; set; }

    }
}
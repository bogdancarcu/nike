﻿using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using System.ComponentModel.DataAnnotations;

namespace Nike.Models.Blocks
{
    [ContentType(DisplayName = "Footer Block", GUID = "94047f27-b60f-478f-8c21-7e5f788a2198", Description = "")]
    public class FooterBlock : BlockData
    {

        [CultureSpecific]
        [Display(
            Name = "Categories",
            Description = "Name field's description",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        public virtual LinkItemCollection Categories { get; set; }


        [CultureSpecific]
        [Display(
          Name = "Information",
          Description = "Name field's description",
          GroupName = SystemTabNames.Content,
          Order = 2)]
        public virtual LinkItemCollection Information { get; set; }
    }
}
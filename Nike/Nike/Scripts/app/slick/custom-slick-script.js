$(".slidesubtitle").html(function () {
    var text = $(this).text().split(" ")[0];
    var fulltext = $(this).text().split(" ");
    var bolded = text.bold();
    fulltext[0] = bolded;
    return fulltext.join(" ");
});

$(document).ready(function () {

    //arrows
    var width = $(window).width();
    var height = $(window).height();
    var decision1 = $('#desktopNeeds').val();
    var decision2 = $('#mobileNeeds').val();

    if (height > 853 && width > 576) {

        //desktop
        if (decision1 === 'true') {

            $('#stealthArrows').css('visibility', 'visible');
            $('#stealthLine').css('visibility', 'visible');

        } else {

            $('#stealthArrows').remove();
            $('#stealthLine').remove();

        }

    } else if (decision1 === 'true' || decision2 === 'true') {

        //mobile
        $('#stealthArrows').css('visibility', 'visible');
        $('#stealthLine').css('visibility', 'visible');

    } else
    {
        $('#stealthArrows').remove();
        $('#stealthLine').remove();
    }


    //init to remove flickering
    $('.slider').on('init', function (event, slick) {
        $(".slider").css('visibility', 'visible');
    }); 

    //slick
    $('.slider').slick({
       
        dots: false,
        arrows: false,

        infinite: false,
        cssEase: 'linear',
        centerModee: true,
        centerPadding: '0',
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1920, // desktop breakpoint
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                  
                }
            },
            {
                breakpoint: 756, // tablet breakpoint
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480, // mobile breakpoint
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
});


$('.left').click(function () {
    $('.slider').slick('slickPrev');
})

$('.right').click(function () {
    $('.slider').slick('slickNext');
})



﻿$(document).on('click', 'button[data-id]', function () {
    var url = '../../../ShoppingCartItemList/RemoveItem'
    var id = $(this).attr('data-id');
    var input_id = "input[name=input-remove-".concat(id).concat("]");
    var val = $(input_id).val();
    var skuCode = val;
    var myData = { skuCode: skuCode};
    $.ajax({
        type: 'POST',
        url: url,
        data: JSON.stringify(myData),
        contentType: 'application/json',
        cache: false,
        dataType: 'json',
        success: function (data) {

            $('#'.concat(data.Code)).hide();
            $('#desc-'.concat(data.Code)).hide();
            $('#row-'.concat(data.Code)).hide();
            $('#br1-'.concat(data.Code)).hide();
            $('#br2-'.concat(data.Code)).hide();

            document.getElementById("cart-summary-total-price").innerHTML = '$' + parseFloat(data.Price).toFixed(2);

            if (data.Price == 0) {
                $("#hidden-when-empty").addClass("hidden");
                $("#text-when-empty").removeClass("hidden");
                $("#image-when-empty").removeClass("hidden");

            } else {
                $("#hidden-when-empty").removeClass("hidden");
                $("#text-when-empty").addClass("hidden");
                $("#image-when-empty").addClass("hidden");
            }
            updateMiniCart();
        },

        error: function (error) {
        },

    });

});

function computeItemTotalPrice(idItemCounter) {
    var idItemQuantity = "item-quantity" + idItemCounter;
    var idItemTotalPrice = "item" + idItemCounter;
    var idItemPrice = "item-price" + idItemCounter;
    var idItemErrorMessage = "errormessage" + idItemCounter;
    var skuCode = document.getElementById("quantity-changer-".concat(idItemCounter)).value;
    var quantity = document.getElementById(idItemQuantity).value;
    var itemPrice = document.getElementById(idItemPrice).innerHTML;
    if (validateQuantity(quantity, idItemErrorMessage)) {

        var url = '../../../ShoppingCartItemList/UpdateItemQuantity';
        var myData = { skuCode: skuCode, quantity: quantity };

        $.ajax({
            type: 'POST',
            url: url,
            data: JSON.stringify(myData),
            contentType: 'application/json',
            cache: false,
            success: function (data) {

                var finalPrice = data.Quantity * data.Price;
                document.getElementById(idItemTotalPrice).innerHTML = '$' + parseFloat(finalPrice).toFixed(2);
                document.getElementById("cart-summary-total-price").innerHTML = '$' + parseFloat(data.TotalPrice).toFixed(2);
                updateMiniCart();
            },

            error: function (error) {
            },
        });
        document.getElementById(idItemErrorMessage).innerHTML = "";
    }
 }

function computeTotalPrice() {
    $(document).ready(function () {
        var url = '../../../ShoppingCartItemList/GetTotalPriceCartSummary';

        $.ajax({
            type: 'POST',
            url: url,
            contentType: 'application/json',
            cache: false,
            success: function (data) {
                ClearCartPageOnEmptyCart();
                updateMiniCart();
            },

            error: function (error) {
            },
        });
    });
    
}
function ClearCartPageOnEmptyCart() {
    if ($("#itemNr").text() > 0) {
        $("#when-cart-is-empty").css("display", "none");
        $("#text-when-empty").css("display", "none");
        $("#image-when-empty").css("display", "none");
    } else {
        $("#when-cart-is-empty").removeClass("hidden");
        $("#when-cart-is-empty").css("display", "block")
        $("#text-when-empty").css("display", "block");
        $("#image-when-empty").css("display", "block");
        $("#hidden-when-empty").css("display", "none");
    }
}
$(document).ready(function () {
    ClearCartPageOnEmptyCart();
});
function validateQuantity(quantity, idItemErrorMessage) {
    if (!quantity.match(/^[1-9]\d*$/)) {
        document.getElementById(idItemErrorMessage).innerHTML = "Invalid format for quantity: please type positive integers only";
        return false;
    }
    return true;
}


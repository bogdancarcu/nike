﻿$(document).ready(function () {
    updateMiniCart();
    if ($("#not").html() == "0") $("#not").hide();
    
});

function RedirectPage() {
    var mini_url = "../../../Layout/RedirectToShoppingCartPage"
    $.ajax({
        type: 'POST',
        url: mini_url,
        cache: false,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            var url = data.url;
            window.location.href = url;   
        },
        error: function (error) {
        },
    });
}

function updateMiniCart() {
    var mini_url = "../../../Layout/MiniCart"
    $.ajax({
        type: 'POST',
        url: mini_url,
        contentType: 'application/json',
        cache: false,
        dataType: 'json',
        success: function (data) {
    
            var pricesToUpdate = document.getElementsByClassName('totalprice');
            var itemNumbersToUpdate = document.getElementsByClassName('itemNr');

            for (var i = 0; i < pricesToUpdate.length; i++) {
                pricesToUpdate[i].textContent = parseFloat(data.TotalPrice).toFixed(2);
            }
            for (var i = 0; i < itemNumbersToUpdate.length; i++) {
                itemNumbersToUpdate[i].textContent = data.NumberOfItems;
            }

        },
        error: function (error) {
        },
        complete: function () {
            ClearCartPageOnEmptyCart();
        }
    });
}

$("[data-toggle=popover-2]").popover({
    content: function () {
        return $('#popover-content-2').html();
    }

});

$('.pop-2').popover({
    html: true,
    trigger: 'manual',
    container: $(this).attr('id'),
    placement: 'top',
    style: {
        pointerEvents: 'auto'
    },
    content: function () {
        $return = '<div class="hover-hovercard"></div>';
    }
}).on("mouseenter", function () {
    var _this = this;
    $(this).popover("show");
    $(this).siblings(".popover").on("mouseleave", function () {
        $(_this).popover('hide');
    });
}).on("mouseleave", function () {
    var _this = this;
    setTimeout(function () {
        if (!$(".popover:hover").length) {
            $(_this).popover("hide")
        }
    }, 100);
});


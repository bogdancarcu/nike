﻿$(document).ready(function () {

    //first image being selected
    $('#0').attr('checked', true);

    $('body').on('click', 'input[name="color"]:radio', function () {
        var checked = $("input[name='color']:checked").val();

        var url = '../../../ShoeProduct/DisplayCurrentVariants';
        var shoeProductViewModel = JSON.parse($("#model").attr("data-model"));
        var myData = { shoeProductViewModel: shoeProductViewModel, color: checked };

        $.ajax({
            type: 'POST',
            url: url,
            data: JSON.stringify(myData),
            contentType: 'application/json',
            cache: false,
            success: function (data) {
                $('.partialArea').html(data);
            },

            error: function (error) {
            },

            complete: function () {
                //make the color remain selected
                $('#'.concat(checked).replace(/\s/g, "")).attr('checked', true);
                //first image being selected again
                $('#0').attr('checked', true);
            }

        });
    });

    $('body').on('click', 'input[name="size"]:radio', function () {
        var checked = $("input[name='size']:checked").val();

        var color = $("input[name='colorField']").val();
        var url = '../../../ShoeProduct/DisplayCurrentSizeVariant';
        var currentViewModel = JSON.parse($("#model-sku").attr("data-model"));
        var myData = { currentViewModel: currentViewModel, size: checked };

        $.ajax({
            type: 'POST',
            url: url,
            data: JSON.stringify(myData),
            contentType: 'application/json',
            cache: false,
            success: function (data) {
                $('.partialArea').html(data);
            },

            error: function (error) {
            },

            complete: function () {
                //make the color remain selected
                $('#'.concat(color).replace(/\s/g, "")).attr('checked', true);
                //make the size remain selected
                $('#'.concat(checked).replace(/\s/g, "")).attr('checked', true);
                //first image being selected again
                $('#0').attr('checked', true);
                //enable button
                $('#addtocart').attr('disabled', false);
                $('#addtocart').removeClass("transparent-disabled");

                var btn = document.getElementById("addtocart");
                btn.className = "enabled-cart-button";
                tooltip();
            }

        });
    });

    $('body').on('click', 'input[name="image-selector"]:radio', function () {
        $("#main").attr("src", $(this).val());
    });

    tooltip();
});


    function tooltip() {
        $('.selection-confirm').mouseover(function (e) {
            var tip = $(this).attr('title');
            $(this).attr('title', '');
            $(this).append('<div id="tooltip"><div class="tipHeader"></div><div class="tipBody">' + tip + '</div><div class="tipFooter"></div></div>');

            $('#tooltip').fadeIn('500');
            $('#tooltip').fadeTo('10', 0.8);

        }).mousemove(function (e) {

        }).mouseout(function () {
            $(this).attr('title', $('.tipBody').html());
            $(this).children('div#tooltip').remove();
        });
    }
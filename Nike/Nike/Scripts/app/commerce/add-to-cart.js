﻿$(document).ready(function () {

    $('body').on('click', 'button[name="addtocart"]', function () {

        var url = '../../../ShoeProduct/AddToCart';
        var skuCode = JSON.parse($("#model-code").attr("data-model"));
        var myData = { skuCode : skuCode };
       
        $.ajax({
            type: 'POST',
            url: url,
            data: JSON.stringify(myData),
            contentType: 'application/json',
            cache: false,
            success: function (data) {
                updateMiniCart();
            },
            error: function () {
            },

        });
    });
});

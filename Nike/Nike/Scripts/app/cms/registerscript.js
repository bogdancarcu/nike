﻿$('input[type="radio"]').on('change', function (e) {
    var sel = $("input[name='Gender']:checked").val();
    if (sel == 'Female') {
        var x = $(document.getElementById("ic")).removeClass('fa-male').addClass('fa-female');

    } else {
        var x = $(document.getElementById("ic")).removeClass('fa-female').addClass('fa-male');
    }
});

$(document).ready(function () {
    updateLastname();
});

$(document).on('click', '#logout', function () {

    $.ajax({
        type: 'POST',
        url: '../../../RegisterAndLogInPage/SignOut',
        contentType: 'application/json',
        cache: false,
        success: function () {
            updateLastname();
            $(".pop").popover("hide");
            window.location = "../../../";
        },
        error: function () {
        }
    });
 
});

function updateLastname() {
    var mini_url = "../../../Layout/MiniCart"
    $.ajax({
        type: 'POST',
        url: mini_url,
        contentType: 'application/json',
        cache: false,
        dataType: 'json',
        success: function (data) {
           // $("#loggeduser").text(data.Username);
        },
        error: function (error) {
        },
    });
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function validatePhone(p) {
    var phoneRe = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/;
    var digits = p.replace(/\D/g, "");
    return phoneRe.test(digits);
}

function validateForm() {
    
    var firstname = document.forms["registerForm"]["firstname"].value;
    if (firstname == "") {
        document.getElementById("registererror").innerHTML = "*First Name must be filled out!";
        return false;
    }
    var lastname = document.forms["registerForm"]["lastname"].value;
    if (lastname == "") {
        document.getElementById("registererror").innerHTML = "*Last Name must be filled out!";
        return false;
    }
    var phonenumber = document.forms["registerForm"]["phonenumber"].value;
    if (!validatePhone(phonenumber)) {
        document.getElementById("registererror").innerHTML = "*Incorrect phone number!";
        return false;
    }
    var email = document.forms["registerForm"]["email"].value;
    if (!validateEmail(email)) {
        document.getElementById("registererror").innerHTML = "*Incorrect email address!";
        return false;
    }
    var password = document.forms["registerForm"]["password1"].value;
    var passwordtest = document.forms["registerForm"]["password2"].value;
    if (password == "" || passwordtest == "") {
        document.getElementById("registererror").innerHTML = "*Password must be filled out!";
        return false;
    }
    if (password !== passwordtest) {
        document.getElementById("registererror").innerHTML = "*Passwords don't match!";
        return false;
    }
    
    var gender = $('input[name=gender]:checked').val();
    if (gender == null) {
        document.getElementById("registererror").innerHTML = "*Please select your gender!";
        return false;
    }

    var state = $('#dropdown :selected').text();

    var myData = { firstName: firstname, lastName: lastname, gender: gender, state: state, phoneNumber: phonenumber, email: email, password: password };

    $.ajax({
        type: 'POST',
        url: 'AddUser', 
        data: JSON.stringify(myData),
        contentType: 'application/json',
        dataType: 'json',
        cache: false,
        success: function (data) {
            if (data.Error == null) {
                url = "../../../";
                $(location).attr("href", url);
                updateLastname();
            }
            else {
                document.getElementById("registererror").innerHTML = data.Error;
            }
        },
        error: function () {
        }
    });
    return false;
}

function validateLoginForm() {
    var email = document.forms["loginform"]["email"].value;
    if (!validateEmail(email)) {
        document.getElementById("loginerror").innerHTML = "*Incorrect email address!";
        return false;
    }

    var password = document.forms["loginform"]["password"].value;
    if (password == "") {
        document.getElementById("loginerror").innerHTML = "*Password must be filled out!";
        return false;
    }

    var myData = { email: email, password: password };

    $.ajax({
        type: 'POST',
        url: 'GetUser',
        data: JSON.stringify(myData),
        contentType: 'application/json',
        dataType: 'json',
        cache: false,
        success: function (data) {
            if (data.Error == null) {
                url = "../../../";
                $(location).attr("href", url);
                updateLastname();
            } else {
                $(document).ready(function () {
                    $("#loginerror").text(data.Error);
                });
            }
        },
        error: function () {
        }
    });
    return false;
}

$("[data-toggle=popover]").popover({
    content: function () {

        if ($("#loggeduser").text() != "GUEST") {
            return $('#popover-content').html();
        }
    }

});


    $('.pop').popover({
        html: true,
        trigger: 'manual',
        container: $(this).attr('id'),
        placement: 'top',
        style: {
            pointerEvents: 'auto'
        },
        content: function () {
            if ($("#loggeduser").text() != "GUEST") {
                $return = '<div class="hover-hovercard"></div>';
            }
        }
    }).on("mouseenter", function () {
        if ($("#loggeduser").text() != "GUEST") {
            var _this = this;
            $(this).popover("show");
            $(this).siblings(".popover").on("mouseleave", function () {
                $(_this).popover('hide');
            });
        }
    }).on("mouseleave", function () {
        if ($("#loggeduser").text() != "GUEST") {
            var _this = this;
            setTimeout(function () {
                if (!$(".popover:hover").length) {
                    $(_this).popover("hide")
                }
            }, 100);
        }
    });

function RedirectToLoginPage() {
    var mini_url = "../../../Layout/RedirectToLoginPage"
    $.ajax({
        type: 'POST',
        url: mini_url,
        cache: false,
        contentType: 'application/json',
        dataType: 'json',
        success: function (data) {
            var url = data.url;
            window.location.href = url;
        },
        error: function (error) {
        },
    });
}

$(document).on('click', '#register-login', function () {
    RedirectToLoginPage();
});

function handleSuccess(result) {
    if (result.redirectTo) {
        window.location.href = result.redirectTo;
        updateLastname();
    } else {
        $('#register-form').html(result);
    }
}

function handleLoginSuccess(result) {
    if (result.redirectTo) {
        window.location.href = result.redirectTo;
        updateLastname();
    } else {
        $('#login-form').html(result);
        
    }
}
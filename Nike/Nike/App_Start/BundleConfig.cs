﻿using System.Collections.Generic;
using System.Web.Optimization;

namespace Nike.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {

            var bundle = new ScriptBundle("~/bundles/common")
                .Include("~/Scripts/jquery/modernizr-2.6.2.js")
                .Include("~/Scripts/jquery/jquery-3.3.1.js")
                .Include("~/Scripts/bootstrap/bootstrap.js")
                .Include("~/Scripts/umd/popper.js")
                .Include("~/Scripts/app/commerce/pdpscript.js")
                .Include("~/Scripts/app/commerce/add-to-cart.js")
                .Include("~/Scripts/app/commerce/cart-summary.js")
                .Include("~/Scripts/app/commerce/mini-cart.js")
                .Include("~/Scripts/jquery/jquery.unobtrusive-ajax.js")
                .Include("~/Scripts/app/cms/registerscript.js"); 

            bundle.Orderer = new AsIsBundleOrderer();
            bundles.Add(bundle);
      
            var bundle_slick = new ScriptBundle("~/bundles/slickJS")
                  .Include("~/Scripts/slick/slick.js")
                  .Include("~/Scripts/app/slick/custom-slick-script.js");

            bundle_slick.Orderer = new AsIsBundleOrderer();
            bundles.Add(bundle_slick);

            bundles.Add(new StyleBundle("~/bundles/layoutCSS")
                .Include("~/Content/bootstrap.min.css")
                .Include("~/Styles/Style.css")
                .Include("~/Styles/NavbarStyle.css"));

            
            bundles.Add(new StyleBundle("~/bundles/slickStyle")
                .Include("~/Styles/Slick/slick.css")
                .Include("~/Styles/Slick/slick-theme.css"));


            bundles.Add(new StyleBundle("~/bundles/heroBlock") 
                .Include("~/Styles/HeroBlockStyle.css")
                .Include("~/Styles/GalleryHeroBlockStyle.css"));

            bundles.Add(new StyleBundle("~/bundles/commerceCSS")
                 .Include("~/Styles/Commerce/Product.css")
                 .Include("~/Styles/Commerce/ShoppingCart.css")
                 .Include("~/Styles/Commerce/Variant.css")
                 .Include("~/Styles/Commerce/MiniCart.css")
                 .Include("~/Styles/RegisterStyle.css"));
        }
    }

    class AsIsBundleOrderer : IBundleOrderer
    {
        public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            return files;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;

namespace Nike.Validators
{
    public class CustomLoginEmailValidator : ValidationAttribute
    {

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                string email = value.ToString();

                if (Regex.IsMatch(email, @"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", RegexOptions.IgnoreCase))
                {
                    if (Membership.GetUser(email) != null)
                    {
                        return ValidationResult.Success;
                    }
                    else
                    {
                        return new ValidationResult("That email is not registered with an account");
                    }
                }
                else
                {
                    return new ValidationResult("Please Enter a Valid Email");
                }
            }
            else
            {
                return new ValidationResult("" + validationContext.DisplayName + " is required");
            }
        }
         
    }
}